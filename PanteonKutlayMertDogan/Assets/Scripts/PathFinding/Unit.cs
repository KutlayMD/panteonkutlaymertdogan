﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour {


	public Transform target;

	public float speed;
	
	
	Vector3[] path;
	int targetIndex;
	

	void Start() {
		speed = Random.Range(6,9);
		PathRequestManager.RequestPath(transform.position,target.position, OnPathFound);
		StartCoroutine(RepeatPathRequest());
	}

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
		if (pathSuccessful) {
			path = newPath;
			targetIndex = 0;
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

	IEnumerator FollowPath() {

        if (path.Length > 0)
        {
			Vector3 currentWaypoint = path[0];
			while (true)
			{
				if (transform.position == currentWaypoint)
				{
					targetIndex++;
					if (targetIndex >= path.Length)
					{
						yield break;
					}
					currentWaypoint = path[targetIndex];
				}

				transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
				yield return null;

			}
		}
        else
        {
			yield return null;
		}
		
	}

	IEnumerator RepeatPathRequest()
    {
		yield return new WaitForSeconds(0.5f);
		PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
		StartCoroutine(RepeatPathRequest());
    }

	public void OnDrawGizmos() {
		if (path != null) {
			for (int i = targetIndex; i < path.Length; i ++) {
				Gizmos.color = Color.black;
				Gizmos.DrawCube(path[i], Vector3.one);

				if (i == targetIndex) {
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else {
					Gizmos.DrawLine(path[i-1],path[i]);
				}
			}
		}
	}
}
