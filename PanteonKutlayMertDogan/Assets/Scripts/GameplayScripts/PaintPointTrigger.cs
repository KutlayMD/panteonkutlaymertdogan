﻿using UnityEngine;

public class PaintPointTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerMovement.instance.SetCanMoveWithPaintTrigger(3,false,true);
            UIController.instance.SetNextButtonActive(true);
            Destroy(gameObject);
        }
    }
}
