﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject paintedwall,painter;

    public GameObject restartbuttonobject, playbuttonobject, quitbuttonobject;

    public Text ranktext;

    public static UIController instance;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    void Start()
    {
        instance = this;  
    }

    public void OnClickPlayButton()
    {
        Time.timeScale = 1;
        playbuttonobject.SetActive(false);
    }

    public void OnClickRestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnQuitButtonClick()
    {
        Application.Quit();
    }

    public void SetNextButtonActive(bool activate)
    {
        restartbuttonobject.SetActive(activate);
        quitbuttonobject.SetActive(activate);
    }

    public void UpdateRankText(int rank, int playercount)
    {
        ranktext.text = rank + " / " + playercount;
    }
}
