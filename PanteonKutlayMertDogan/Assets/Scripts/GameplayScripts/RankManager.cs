﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RankManager : MonoBehaviour
{

    public Dictionary<string, float> players;

    Dictionary<string, float> sortedPlayers;


    public static RankManager instance;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        players = new Dictionary<string, float>();
    }


    public void UpdateDistanceAndRank(string name, float distance)
    {

        if (!players.ContainsKey(name))
        {
            players.Add(name, distance);
        }

        int i = 1;

        players[name] = distance;

        IOrderedEnumerable<KeyValuePair<string, float>> sortedPlayer = players.OrderBy(x => x.Value);

        foreach (KeyValuePair<string, float> dic in sortedPlayer)
        {
            if (dic.Key == "Boy")
            {
                UIController.instance.UpdateRankText(i,sortedPlayer.Count());
                break;
            }
            i++;
        }
    }
}
