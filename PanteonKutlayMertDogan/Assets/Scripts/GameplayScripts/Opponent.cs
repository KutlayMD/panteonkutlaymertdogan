﻿using System.Collections;
using UnityEngine;

public class Opponent : MonoBehaviour
{
    public Transform target;


    private Vector3 _startpos;

    private Animator _anim;


    void Start()
    {
       

        _startpos = transform.position;
        _anim = GetComponent<Animator>();

        StartCoroutine(UpdateRankAndDistance());
    }

    void Update()
    {
        if (transform.position.y < -3)
        {
            transform.position = _startpos;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PaintPoint"))
        {
            GetComponent<Unit>().speed = 0;
            _anim.SetBool("Run", false);
            _anim.SetBool("Idle",true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Obstacle"))
        {
            _anim.SetTrigger("Fall");
            GetComponent<Unit>().speed = 0;
            StartCoroutine(TurnStartPoint());
        }
    }

    IEnumerator TurnStartPoint()
    {
        yield return new WaitForSeconds(1);
        transform.position = _startpos;
        GetComponent<Unit>().speed = Random.Range(6, 9);

    }

    IEnumerator UpdateRankAndDistance()
    {
        yield return new WaitForSeconds(0.2f);
        RankManager.instance.UpdateDistanceAndRank(gameObject.name, (target.position - transform.position).magnitude);
        StartCoroutine(UpdateRankAndDistance());
    }
}
