﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    private Touch _touch;
    private float _speed = 5f;
    private Vector2 _startPos;
    private Vector2 _direction;
    private Vector2 _normdirection;
    private Vector3 _startposition;
    private bool _rotatingplatform = false;
    private bool _move = false;//checks if there is input
    private bool _canmove = true;//checks if player fall.
    private bool _painting = false;

    public Rigidbody rb;
    public Animator anim;

    public GameObject painter;

    public Transform finishline;

    public static PlayerMovement instance;


    void Start()
    {
        instance = this;

        _startposition = transform.position;

        StartCoroutine(UpdateRankAndDistance());
    }

    void Update()
    {

        if (transform.position.y < -3)
        {
            TurnStartPosition();
        }

        if (_canmove)
        {
        _touch = Input.GetTouch(0);

        if (_touch.phase == TouchPhase.Began)
        {
             rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            _startPos = _touch.position;
            _move = true;
        }

        if (_touch.phase == TouchPhase.Moved)
        {
            _direction = _touch.position - _startPos;
            _normdirection = _direction.normalized;
            Move(_normdirection);
        }

        if (_touch.phase == TouchPhase.Ended)
        {
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            StopMovement();
        }

         if (_painting)
        {
            if (_touch.phase == TouchPhase.Began)
            {
                anim.SetBool("Paint",true);
            }

            if (_touch.phase == TouchPhase.Ended)
            {
                anim.SetBool("Paint",false);
            }
        }
        }
    }

    private void FixedUpdate()
    {
        if (_direction.magnitude >= 5 && _move)
        {
            _normdirection = _direction.normalized;
            Move(_normdirection);
        }
    }

    void Move(Vector2 direct)
    {
        Vector3 rotpos = transform.position - new Vector3(direct.x + transform.position.x, transform.position.y, direct.y + transform.position.z);
        anim.SetBool("Run", true);

        Quaternion rotation = Quaternion.LookRotation(-1 * rotpos);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10 * Time.fixedDeltaTime);

        if (!_rotatingplatform)
        {
            var v3 = transform.forward * _speed;
            v3.y = rb.velocity.y;
            rb.velocity = v3;
        }
        else
        {
            var nvelocity = transform.forward * _speed;
            nvelocity.x += -Vector3.right.x * 3;
            nvelocity.y = rb.velocity.y;
            rb.velocity = nvelocity;
        }
    }

    void StopMovement()
    {
        _move = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        anim.SetBool("Run", false);
    }

    public void TurnStartPosition()
    {
        transform.position = _startposition;
    }

    public void SetMovementType(bool rotatingplatform)
    {
        _rotatingplatform = rotatingplatform;
    }

    public void SetCanMoveWithFall(bool canmove)
    {
        _canmove = canmove;
        if (!_canmove)
        {
            anim.SetTrigger("Fall");
            anim.SetBool("Idle", false);
            StopMovement();
        }

        StartCoroutine(WaitForFallAnimation());
    }

    public void SetCanMoveWithPaintTrigger(int camyaxisvalue, bool canmove, bool ispainting)
    {
        CameraFollow.instance.offset = new Vector3(CameraFollow.instance.offset.x, camyaxisvalue, CameraFollow.instance.offset.z);
        painter.SetActive(ispainting);
        _canmove = canmove;
        _painting = ispainting;
        if (!_canmove)
        {
            StopMovement();
        }
        else
        {
            anim.SetBool("Paint", ispainting);
        }
    }

    IEnumerator WaitForFallAnimation()
    {
        yield return new WaitForSeconds(1);//animasyonun süresi ile değiştirilecek.
        _canmove = true;
        anim.SetBool("Idle",true);
        TurnStartPosition();
    }

    IEnumerator UpdateRankAndDistance()
    {
        yield return new WaitForSeconds(0.2f);
        RankManager.instance.UpdateDistanceAndRank(gameObject.name, (finishline.position - transform.position).magnitude);
        StartCoroutine(UpdateRankAndDistance());
    }
}
