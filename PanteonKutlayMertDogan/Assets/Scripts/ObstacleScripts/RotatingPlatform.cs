﻿using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    public float speed;

    void FixedUpdate()
    {
        transform.Rotate(Vector3.forward, speed, Space.World);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetMovementType(true);//true; rotatingplatform movement
        }
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetMovementType(false);//false; normal movement
        }
    }

}
