﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfDonut : MonoBehaviour
{
    public float speed;

    private bool _canrotate = false;

    void Start()
    {
        StartCoroutine(RotateInTime());
    }


    void FixedUpdate()
    {
        if (_canrotate)
        {
            transform.Rotate(Vector3.right, speed, Space.World);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetCanMoveWithFall(false);
        }
    }


    IEnumerator RotateInTime()
    {
        float i;

        if (_canrotate)
        {
            i = Random.Range(5, 8);
        }
        else
        {
            i = Random.Range(3, 5);
        }
        yield return new WaitForSeconds(i);
        _canrotate = !_canrotate;
        StartCoroutine(RotateInTime());
    }
}
