﻿using UnityEngine;

public class RotatingStick : MonoBehaviour
{
    private Transform parenttransform;

    public float speed;

    public GameObject parentobject;

    // Start is called before the first frame update
    void Start()
    {
        parenttransform = GetComponentInParent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*parenttransform.transform.Rotate(Vector3.up*speed);
        parenttransform.transform.eulerAngles = transform.eulerAngles;*/
        parentobject.transform.Rotate(Vector3.up*speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetCanMoveWithFall(false);
        }
    }
}
