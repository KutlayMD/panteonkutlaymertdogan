﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizantalObstacle : MonoBehaviour
{
    public float maxxvalue, minxvalue;

    public float speed;

    public bool dir;

    private Vector3 _destination;

    // Start is called before the first frame update
    void Start()
    {
        if (dir)
        {
            _destination = new Vector3(maxxvalue,transform.position.y,transform.position.z);
        }
        else
        {
            _destination = new Vector3(minxvalue, transform.position.y, transform.position.z);
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HorizantalMovement();
    }

    void HorizantalMovement()
    {
        if (dir)
        {
            transform.position = Vector3.MoveTowards(transform.position, _destination, Time.fixedDeltaTime * speed);
            if ((transform.position - _destination).magnitude <= 0)
            {
                dir = false;
                _destination = _destination = new Vector3(minxvalue, transform.position.y, transform.position.z);
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, _destination, Time.fixedDeltaTime * speed);
            if ((transform.position - _destination).magnitude <= 0)
            {
                dir = true;
                _destination = _destination = new Vector3(maxxvalue, transform.position.y, transform.position.z);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerMovement.instance.SetCanMoveWithFall(false);
        }
    }
}
